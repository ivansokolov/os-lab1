#include <utility>
extern "C" {
#include "param.h"
#include "memlayout.h"
#include "types.h"
#include "riscv.h"
#include "spinlock.h"
#include "proc.h"
#include "defs.h"
}

class Spinlock_guard {
  spinlock* lock;
public:
  explicit Spinlock_guard (spinlock* l): lock(l) {
    if (lock)
      acquire(lock);
  }

  ~Spinlock_guard () {
    if (lock)
      release(lock);
  }

  Spinlock_guard (Spinlock_guard&& other): lock(std::exchange(other.lock, nullptr)) {}
  Spinlock_guard (const Spinlock_guard&) = delete;
  auto operator= (auto&&) = delete;
};

static constexpr auto get_trapframe_register_offset (int reg)
{
  constexpr uint64 trapframe::* offsets[] = {
    // omit s0 and s1
    &trapframe::s2,
    &trapframe::s3,
    &trapframe::s4,
    &trapframe::s5,
    &trapframe::s6,
    &trapframe::s7,
    &trapframe::s8,
    &trapframe::s9,
    &trapframe::s10,
    &trapframe::s11,
  };
  return offsets[reg-2];
}

static void dump_proc_registers (const proc* p)
{
  for (int reg_id = 2; reg_id < 12; reg_id++) {
    printf("  s%d\t= %d\n",
        reg_id,
        p->trapframe->*get_trapframe_register_offset(reg_id));
  }
}

static std::pair<proc*, Spinlock_guard> find_proc (int pid)
{
  extern proc proc[NPROC];
  for (auto& p: proc) {
    auto guard = Spinlock_guard(&p.lock);
    if (p.pid == pid)
      return { &p, std::move(guard) };
  }
  return { nullptr, Spinlock_guard(nullptr) };
}


enum class Dump2_result: int64 {
  ok = 0,
  no_privilege = -1,
  bad_pid = -2,
  bad_reg_id = -3,
  bad_ret_addr = -4,
};

static Dump2_result dump2 (int pid, int reg_id, uint64 ret_addr)
{
  using enum Dump2_result;

  if (reg_id < 2 || reg_id > 11)
    return bad_reg_id;

  auto global_guard = Spinlock_guard(&wait_lock);
  auto [examined_proc, examined_guard] = find_proc(pid);
  if (!examined_proc)
    return bad_pid;

  auto calling_proc = myproc();

  // If we do not find the calling process in the examined process's
  // ancestor chain, we cannot authorize the dump
  for (auto c = examined_proc; c != calling_proc; c = c->parent) {
    if (c == nullptr)
      return no_privilege;
  }

  // Avoid trying to lock twice in case we're examining ourselves
  auto calling_guard =
    Spinlock_guard(calling_proc == examined_proc ? nullptr : &calling_proc->lock);

  uint64 reg = examined_proc->trapframe->*get_trapframe_register_offset(reg_id);
  auto ptr = reinterpret_cast<char*>(&reg);

  if (copyout(calling_proc->pagetable, ret_addr, ptr, 8) == -1)
    return bad_ret_addr;

  return ok;
}


// ==============================================================

extern "C" uint64 sys_dump ()
{
  auto calling_proc = myproc();
  auto guard = Spinlock_guard(&calling_proc->lock);
  dump_proc_registers(calling_proc);
  return 0;
}

extern "C" uint64 sys_dump2 ()
{
  int pid;
  int reg_id;
  uint64 ret_addr;
  argint(0, &pid);
  argint(1, &reg_id);
  argaddr(2, &ret_addr);
  return static_cast<uint64>(dump2(pid, reg_id, ret_addr));
}
