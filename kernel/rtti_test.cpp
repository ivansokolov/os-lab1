#include <typeinfo>
#include <utility>
extern "C" {
#include "param.h"
#include "memlayout.h"
#include "types.h"
#include "riscv.h"
#include "spinlock.h"
#include "proc.h"
#include "defs.h"
}

namespace {
struct Base {
  virtual int get () const = 0;
};

template<int I>
struct Derived: Base {
  int get () const override { return I; }
  inline static constexpr Derived instance {};
};

const Base& get_ref (int class_id)
{
  switch (class_id) {
  case 0: return Derived<0>::instance;
  case 1: return Derived<1>::instance;
  case 2: return Derived<2>::instance;
  case 3: return Derived<3>::instance;
  default: panic("Invalid rtti test value");
  }
}
} // anon namespace

extern "C" uint64 sys_rtti_test ()
{
  int class_id;
  argint(0, &class_id);
  auto& base = get_ref(class_id);

  printf("type name: %s\n", typeid(base).name());

  if (dynamic_cast<const Derived<2>*>(&base))
    return 123;
  else
    return base.get();
}
