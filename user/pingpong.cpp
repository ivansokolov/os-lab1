#include "kernel/types.h"
#include "user/user.h"
#include <span>
#include <string_view>
#include <utility>

constexpr int stdin = 0;
constexpr int stdout = 1;
constexpr int stderr = 2;

namespace util {

static void fatal [[noreturn]] (std::string_view msg)
{
  write(stderr, msg.data(), msg.size());
  exit(1);
}

static std::span<const std::byte> sv_as_bytes (std::string_view sv)
{
  return { reinterpret_cast<const std::byte*>(sv.data()), sv.size() };
}

static std::string_view bytes_as_sv (std::span<const std::byte> bytes)
{
  return { reinterpret_cast<const char*>(bytes.data()), bytes.size() };
}

}

namespace fd {
class Pipe {
  int fds[2];
  int consume_read_end () { return std::exchange(fds[0], -1); }
  int consume_write_end () { return std::exchange(fds[1], -1); }

public:
  Pipe () {
    if (::pipe(fds) == -1)
      util::fatal("pipe failed");
  }

  ~Pipe () {
    for (int fd: fds) {
      if (fd != -1)
        close(fd);
    }
  }

  int as_read () && {
    close(consume_write_end());
    return consume_read_end();
  }

  int as_write () && {
    close(consume_read_end());
    return consume_write_end();
  }
};

class Endpoint {
  int read_from;
  int write_to;
public:
  Endpoint (int r, int w): read_from(r), write_to(w) {}

  void read_into (std::span<std::byte> data) {
    if (read(read_from, data.data(), data.size()) == -1)
      util::fatal("read failed");
  }

  void write (std::span<const std::byte> data) {
    if (::write(write_to, data.data(), data.size()) == -1)
      util::fatal("read failed");
  }
};
}

static void parent (fd::Endpoint rw)
{
  rw.write(util::sv_as_bytes("ping"));

  std::byte pong_buffer[4];
  rw.read_into(pong_buffer);
  auto pong = util::bytes_as_sv(pong_buffer);
  fprintf(stderr, "Parent [%d] got response: %S\n", getpid(), pong.data(), pong.size());

  if (pong != "pong")
    util::fatal("Wrong response from child");
}

static void child (fd::Endpoint rw)
{
  std::byte ping_buffer[4];
  rw.read_into(ping_buffer);

  auto ping = util::bytes_as_sv(ping_buffer);
  fprintf(stderr, "Child [%d] got message: %S\n", getpid(), ping.data(), ping.size());
  if (ping != "ping")
    util::fatal("Wrong message from parent");

  rw.write(util::sv_as_bytes("pong"));
}

int main (int, char**)
{
  fd::Pipe p2c; // parent writes, child reads
  fd::Pipe c2p; // child writes, parent reads
  if (int child_pid = fork()) {
    parent(fd::Endpoint(
        std::move(c2p).as_read(),
        std::move(p2c).as_write()));
    if (wait(nullptr) != child_pid)
      util::fatal("wait failed");
  } else {
    child(fd::Endpoint(
        std::move(p2c).as_read(),
        std::move(c2p).as_write()));
  }
  return 0;
}
